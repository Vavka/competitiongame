package org.example.mapper;

import org.example.api.v1.dto.UserDto;
import org.example.entity.UserEntity;

public interface UserMapper {

  UserDto toDto(UserEntity userEntity);
}
