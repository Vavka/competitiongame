package org.example.mapper;

import org.example.api.v1.dto.UserInTeamDto;
import org.example.entity.UserInCompetitionEntity;

public interface UserInTeamMapper {

  UserInTeamDto toDto(UserInCompetitionEntity entity);
}
