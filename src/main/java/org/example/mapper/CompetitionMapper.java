package org.example.mapper;

import org.example.api.v1.dto.CompetitionDto;
import org.example.entity.CompetitionEntity;

public interface CompetitionMapper {

  CompetitionDto toDto(CompetitionEntity entity);

  CompetitionEntity toEntity(CompetitionDto dto);
}
