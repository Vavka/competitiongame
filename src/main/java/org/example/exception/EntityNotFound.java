package org.example.exception;
/*
* Потом можно разбить по типам энтити, если нужно будет или передавать коды ошибок
* */
public class EntityNotFound extends RuntimeException {
  public EntityNotFound(String errorMessage) {
    super(errorMessage);
  }

  public EntityNotFound() {
    super();
  }
}
