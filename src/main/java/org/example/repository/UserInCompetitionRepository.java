package org.example.repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.example.entity.UserInCompetitionEntity;

public interface UserInCompetitionRepository {

  Optional<UserInCompetitionEntity> findByUserIdAndCompetitionId(UUID userId, UUID competitionId);

  List<UserInCompetitionEntity> findByTeamIdAndCompetitionId(UUID userId, UUID competitionId);

  UserInCompetitionEntity update(UserInCompetitionEntity userInCompetition);

  UserInCompetitionEntity create(UserInCompetitionEntity entity);

  boolean isExistUserIdAndCompetitionId(UUID userId, UUID competitionId);
}
