package org.example.repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.example.entity.CompetitionEntity;

public interface CompetitionRepository {

  Optional<CompetitionEntity> getById(UUID id);

  List<CompetitionEntity> getAllIsActive();

  CompetitionEntity create(CompetitionEntity entity);

  List<CompetitionEntity> getAllNotFinished();

}
