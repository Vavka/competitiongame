package org.example.service.impl;

import java.math.BigDecimal;
import java.util.TreeSet;
import java.util.UUID;

import org.example.entity.UserInCompetitionEntity;
import org.example.exception.EntityNotFound;
import org.example.repository.UserInCompetitionRepository;
import org.example.service.CompetitionService;
import org.example.service.TeamInCompetitionService;
import org.example.service.UserInCompetitionService;
import org.example.service.UserService;

public class UserInCompetitionServiceImpl implements UserInCompetitionService {

  private UserService userService;
  private CompetitionService competitionService;
  private UserInCompetitionRepository userInCompetitionRepository;
  private TeamInCompetitionService teamInCompetitionService;

  @Override
  public UserInCompetitionEntity addPointForUserInCompetition(
      UUID userId, UUID competitionId, BigDecimal points) {
    var userInCompetition =
        userInCompetitionRepository
            .findByUserIdAndCompetitionId(userId, competitionId)
            .orElseThrow(EntityNotFound::new);
    userInCompetition.setPoints(userInCompetition.getPoints().add(points));
    return userInCompetitionRepository.update(userInCompetition);
  }

  @Override
  public TreeSet<UserInCompetitionEntity> getTopByUserAndCompetition(
      UUID userId, UUID competitionId) {
    var userInCompetition =
        userInCompetitionRepository
            .findByUserIdAndCompetitionId(userId, competitionId)
            .orElseThrow(EntityNotFound::new);
    return new TreeSet<>(
        userInCompetitionRepository.findByTeamIdAndCompetitionId(
            userInCompetition.getTeamId(), userInCompetition.getCompetitionId()));
  }

  @Override
  public UserInCompetitionEntity create(UserInCompetitionEntity entity) {
    return userInCompetitionRepository.create(entity);
  }

  @Override
  public boolean isUserInCompetition(UUID userId, UUID competitionId) {
    return userInCompetitionRepository.isExistUserIdAndCompetitionId(userId, competitionId);
  }
}
