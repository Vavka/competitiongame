package org.example.service.impl;

import java.math.BigDecimal;
import java.util.UUID;

import org.example.entity.TeamInCompetitionEntity;
import org.example.entity.UserInCompetitionEntity;
import org.example.service.CompetitionOperationService;
import org.example.service.CompetitionService;
import org.example.service.TeamInCompetitionService;
import org.example.service.UserInCompetitionService;
import org.example.service.UserService;

public class CompetitionOperationServiceImpl implements CompetitionOperationService {

  private UserService userService;
  private CompetitionService competitionService;
  private UserInCompetitionService userInCompetitionService;
  private TeamInCompetitionService teamInCompetitionService;

//  обязательно нужны транзакции. И оптимистичные локи, что бы все было хорошо
  @Override
  public void addUserInCompetition(UUID userId, UUID competitionId) {
    var user = userService.getById(userId);
    var competition = competitionService.getById(competitionId);
    if (userInCompetitionService.isUserInCompetition(userId, competitionId)) {
      throw new RuntimeException(
          "User already in Competition! User id " + userId + " competition id " + competitionId);
    }
    var team =
        teamInCompetitionService
            .getFirstFreeTeamByCompetition(competition.getId(), competition.getTeamSize())
            .orElseGet(
                () ->
                    teamInCompetitionService.create(
                        TeamInCompetitionEntity.builder()
                            .size(0)
                            .competitionId(competition.getId())
                            .build()));
    team.setSize(team.getSize() + 1);
    teamInCompetitionService.update(team);
    userInCompetitionService.create(
        UserInCompetitionEntity.builder()
            .teamId(team.getId())
            .points(BigDecimal.ZERO)
            .competitionId(competition.getId())
            .userId(user.getId())
            .build());
  }
}
