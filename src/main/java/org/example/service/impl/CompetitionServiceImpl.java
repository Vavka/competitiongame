package org.example.service.impl;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.UUID;

import org.example.api.v1.type.CompetitionStatus;
import org.example.entity.CompetitionEntity;
import org.example.exception.EntityNotFound;
import org.example.repository.CompetitionRepository;
import org.example.service.CompetitionService;
import org.example.service.ScheduledTaskService;

public class CompetitionServiceImpl implements CompetitionService {

  private CompetitionRepository competitionRepository;
  private ScheduledTaskService scheduledTaskService;

  @Override
  public CompetitionEntity getById(UUID id) {
    return competitionRepository.getById(id).orElseThrow(EntityNotFound::new);
  }

  @Override
  public List<CompetitionEntity> getActiveCompetition() {
    return competitionRepository.getAllIsActive();
  }

  @Override
  public CompetitionEntity create(CompetitionEntity entity) {
    //    валидации заполнения полей
    entity.setStatus(CompetitionStatus.PENDING);
    var competition = competitionRepository.create(entity);
    createOrUpdateTask(competition);
    return competition;
  }

  @Override
  public CompetitionEntity update(CompetitionEntity entity) {
    var exist = competitionRepository.getById(entity.getId());
    var competition = competitionRepository.create(entity);
    taskAndInvalidate(competition);
    return competition;
  }

  @Override
  public void invalidateAllTasks() {
    /*
    * Главный метод инвалидации, должен запускаться при пустой очереди планировщика
    * */
    competitionRepository.getAllNotFinished().forEach(this::taskAndInvalidate);
  }

  private void taskAndInvalidate(CompetitionEntity entity) {
    createOrUpdateTask(entity);
    invalidateStatus(entity);
    update(entity);
  }

  private void createOrUpdateTask(CompetitionEntity entity) {
    if (entity.getStartTime().isAfter(ZonedDateTime.now())) {
      scheduledTaskService.addOrUpdateTaskActive(entity.getId(), entity.getStartTime());
    }
    if (entity.getFinishTime().isAfter(ZonedDateTime.now())) {
      scheduledTaskService.addOrUpdateTaskFinished(entity.getId(), entity.getFinishTime());
    }
  }

  private void invalidateStatus(CompetitionEntity entity) {
    //инвалидации граничных условий, если что то пошло не так и планировщик не изменил статус энтити
    if (entity.getStartTime().isBefore(ZonedDateTime.now()) && CompetitionStatus.PENDING.equals(entity.getStatus())) {
      entity.setStatus(CompetitionStatus.ACTIVE);
    }
    if (entity.getFinishTime().isBefore(ZonedDateTime.now()) && CompetitionStatus.ACTIVE.equals(entity.getStatus())) {
      entity.setStatus(CompetitionStatus.FINISHED);
    }
  }
}
