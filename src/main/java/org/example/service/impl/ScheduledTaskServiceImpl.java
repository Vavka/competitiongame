package org.example.service.impl;

import java.time.ZonedDateTime;
import java.util.UUID;

import org.example.service.ScheduledTaskService;

public class ScheduledTaskServiceImpl implements ScheduledTaskService {
/*
* можно использовать Quartz или другой подобный планировщик.
* competitionId является униклальным ключом джобы в планировщике
* */

  @Override
  public void addOrUpdateTaskActive(UUID competitionId, ZonedDateTime startTime) {
    /*
     * Таск на перевод competition в статус Active. Или обновления времени при существовании
     * Если время обновить нельзя - ошибка
     * */
  }

  @Override
  public void addOrUpdateTaskFinished(UUID competitionId, ZonedDateTime finishTime) {
    /*
     * Таск на перевод competition в статус Finished. Или обновления времени при существовании
     * Если время обновить нельзя - ошибка
     * */
  }

  @Override
  public boolean queueTaskIsEmpty() {
    return false;
  }
}
