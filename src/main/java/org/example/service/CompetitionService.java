package org.example.service;

import java.util.List;
import java.util.UUID;

import org.example.entity.CompetitionEntity;

public interface CompetitionService {

  CompetitionEntity getById(UUID id);

  List<CompetitionEntity> getActiveCompetition();

  CompetitionEntity create(CompetitionEntity entity);

  CompetitionEntity update(CompetitionEntity entity);

  void invalidateAllTasks();
}
