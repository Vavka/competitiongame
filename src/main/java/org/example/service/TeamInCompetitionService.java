package org.example.service;

import java.util.Optional;
import java.util.UUID;

import org.example.entity.TeamInCompetitionEntity;

public interface TeamInCompetitionService {

  TeamInCompetitionEntity create(TeamInCompetitionEntity entity);

  TeamInCompetitionEntity update(TeamInCompetitionEntity entity);

  Optional<TeamInCompetitionEntity> getFirstFreeTeamByCompetition(UUID competitionId, int teamSize);
}
