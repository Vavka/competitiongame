package org.example.service;

import java.time.ZonedDateTime;
import java.util.UUID;

public interface ScheduledTaskService {

  void addOrUpdateTaskActive(UUID competitionId, ZonedDateTime startTime);

  void addOrUpdateTaskFinished(UUID competitionId, ZonedDateTime finishTime);

  boolean queueTaskIsEmpty();
}
