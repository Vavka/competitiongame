package org.example.service;

import java.math.BigDecimal;
import java.util.TreeSet;
import java.util.UUID;

import org.example.entity.UserInCompetitionEntity;

public interface UserInCompetitionService {

  UserInCompetitionEntity addPointForUserInCompetition(
      UUID userId, UUID competitionId, BigDecimal points);

  TreeSet<UserInCompetitionEntity> getTopByUserAndCompetition(UUID userId, UUID competitionId);

  UserInCompetitionEntity create(UserInCompetitionEntity entity);

  boolean isUserInCompetition(UUID userId, UUID competitionId);
}
