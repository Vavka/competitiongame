package org.example.service;

import java.util.UUID;

public interface CompetitionOperationService {

  void addUserInCompetition(UUID userId, UUID competitionId);
}
