package org.example.service;

import java.util.UUID;

import org.example.entity.UserEntity;

public interface UserService {

  UserEntity getById(UUID userId);
}
