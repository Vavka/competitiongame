package org.example.api.v1.controller;

import java.util.List;
import java.util.UUID;

import org.example.api.v1.dto.CompetitionDto;
import org.example.api.v1.dto.UsersInTeamResponseDto;

public interface CompetitionControllerV1 {

  UsersInTeamResponseDto addUserToCompetition(UUID userId, UUID competitionId);

  List<CompetitionDto> getActiveCompetition();

  CompetitionDto createCompetition(CompetitionDto dto);

  CompetitionDto update(CompetitionDto dto);
}
