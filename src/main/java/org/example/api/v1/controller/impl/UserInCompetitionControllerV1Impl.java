package org.example.api.v1.controller.impl;

import java.math.BigDecimal;
import java.util.TreeSet;
import java.util.UUID;
import java.util.stream.Collectors;

import org.example.api.v1.controller.UserInCompetitionControllerV1;
import org.example.api.v1.dto.UserInTeamResponseDto;
import org.example.api.v1.dto.UsersInTeamResponseDto;
import org.example.mapper.UserInTeamMapper;
import org.example.service.UserInCompetitionService;

public class UserInCompetitionControllerV1Impl implements UserInCompetitionControllerV1 {

  private UserInCompetitionService userInCompetitionService;
  private UserInTeamMapper userInTeamMapper;

  @Override
  public UsersInTeamResponseDto getTeamTopByUserAndCompetition(UUID userId, UUID competitionId) {
    return UsersInTeamResponseDto.builder()
        .usersInTeamDtos(
            new TreeSet<>(userInCompetitionService.getTopByUserAndCompetition(userId, competitionId).stream()
                .map(userInTeamMapper::toDto)
                .collect(Collectors.toList())))
        .build();
  }

  @Override
  public UserInTeamResponseDto addPointsForUser(
      UUID userId, UUID competitionId, BigDecimal points) {
    return UserInTeamResponseDto.builder()
        .user(
            userInTeamMapper.toDto(
                userInCompetitionService.addPointForUserInCompetition(
                    userId, competitionId, points)))
        .build();
  }
}
