package org.example.api.v1.controller;

import java.math.BigDecimal;
import java.util.UUID;

import org.example.api.v1.dto.UserInTeamResponseDto;
import org.example.api.v1.dto.UsersInTeamResponseDto;

public interface UserInCompetitionControllerV1 {

  UsersInTeamResponseDto getTeamTopByUserAndCompetition(UUID userId, UUID competitionId);

  UserInTeamResponseDto addPointsForUser(UUID userId, UUID competitionId, BigDecimal points);
}
