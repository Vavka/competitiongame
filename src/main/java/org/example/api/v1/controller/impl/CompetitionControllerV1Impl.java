package org.example.api.v1.controller.impl;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import org.example.api.v1.controller.CompetitionControllerV1;
import org.example.api.v1.controller.UserInCompetitionControllerV1;
import org.example.api.v1.dto.CompetitionDto;
import org.example.api.v1.dto.UsersInTeamResponseDto;
import org.example.mapper.CompetitionMapper;
import org.example.service.CompetitionOperationService;
import org.example.service.CompetitionService;

public class CompetitionControllerV1Impl implements CompetitionControllerV1 {

  private CompetitionService competitionService;
  private CompetitionOperationService competitionOperationService;
  private CompetitionMapper competitionMapper;
  private UserInCompetitionControllerV1 userInCompetitionControllerV1;

  @Override
  public UsersInTeamResponseDto addUserToCompetition(UUID userId, UUID competitionId) {
    competitionOperationService.addUserInCompetition(userId, competitionId);
    return userInCompetitionControllerV1.getTeamTopByUserAndCompetition(userId, competitionId);
  }

  @Override
  public List<CompetitionDto> getActiveCompetition() {
    return competitionService.getActiveCompetition().stream()
        .map(competitionMapper::toDto)
        .collect(Collectors.toList());
  }

  @Override
  public CompetitionDto createCompetition(CompetitionDto dto) {
    return competitionMapper.toDto(competitionService.create(competitionMapper.toEntity(dto)));
  }

  @Override
  public CompetitionDto update(CompetitionDto dto) {
    return competitionMapper.toDto(competitionService.update(competitionMapper.toEntity(dto)));
  }
}
