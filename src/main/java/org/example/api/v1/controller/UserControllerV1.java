package org.example.api.v1.controller;

import java.util.UUID;

import org.example.api.v1.dto.UserDto;

public interface UserControllerV1 {

  UserDto getUserById(UUID id);
}
