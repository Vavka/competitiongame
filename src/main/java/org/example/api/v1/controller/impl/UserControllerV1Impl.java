package org.example.api.v1.controller.impl;

import java.util.UUID;

import org.example.api.v1.controller.UserControllerV1;
import org.example.api.v1.dto.UserDto;
import org.example.mapper.UserMapper;
import org.example.service.UserService;

public class UserControllerV1Impl implements UserControllerV1 {

  private UserMapper userMapper;
  private UserService userService;

  @Override
  public UserDto getUserById(UUID id) {
    return userMapper.toDto(userService.getById(id));
  }
}
