package org.example.api.v1.type;

public enum CompetitionStatus {
  PENDING,
  FINISHED,
  ACTIVE
}
