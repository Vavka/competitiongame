package org.example.api.v1.dto;

import java.time.ZonedDateTime;
import java.util.UUID;

import org.example.api.v1.type.CompetitionStatus;

public class CompetitionDto {
  private UUID id;
  private String name;
  private CompetitionStatus status;
  private int teamSize;
  private ZonedDateTime startTime;
  private ZonedDateTime finishTime;
}
