package org.example.api.v1.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@AllArgsConstructor
@Builder
public class UserInTeamResponseDto {
  private UserInTeamDto user;
}
