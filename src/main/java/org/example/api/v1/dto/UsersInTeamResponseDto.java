package org.example.api.v1.dto;

import java.util.TreeSet;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@AllArgsConstructor
@Builder
public class UsersInTeamResponseDto {
  private TreeSet<UserInTeamDto> usersInTeamDtos;
}
