package org.example.api.v1.dto;

import java.math.BigDecimal;
import java.util.UUID;

public class UserInTeamDto implements Comparable<UserInTeamDto> {
  private UUID userId;
  private BigDecimal points;

  @Override
  public int compareTo(UserInTeamDto o) {
    return o.points.compareTo(points);
  }
}
