package org.example.entity;

import java.time.ZonedDateTime;
import java.util.UUID;

import org.example.api.v1.type.CompetitionStatus;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CompetitionEntity {
  private UUID id;
  private String name;
  private CompetitionStatus status;
  private int teamSize;
  private ZonedDateTime startTime;
  private ZonedDateTime finishTime;
}
