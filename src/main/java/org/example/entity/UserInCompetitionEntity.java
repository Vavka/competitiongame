package org.example.entity;

import java.math.BigDecimal;
import java.util.UUID;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserInCompetitionEntity implements Comparable<UserInCompetitionEntity> {
  private UUID id;
  /*
  * При работе с JPA тут будут энтити, соответственно
  * */
  private UUID userId;
  private UUID teamId;
  /* Вообще competitionId тут лишний по классической логике построения связей
  * Но в тз указано несколько кейсов, когда он тут поможет избежать лишних мержей
  * (конкретно действия с очками). Для чистоты можно убрать, но эта оптимизация экономит нам запросы к бд
  * */
  private UUID competitionId;
  private BigDecimal points;


  @Override
  public int compareTo(UserInCompetitionEntity o) {
    return o.points.compareTo(points);
  }
}
